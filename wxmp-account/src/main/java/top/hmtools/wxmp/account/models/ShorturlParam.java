package top.hmtools.wxmp.account.models;

/**
 * 长Url转短URL请求参数实体类
 * @author Hybomyth
 *
 */
public class ShorturlParam {

	/**
	 * 此处填long2short，代表长链接转短链接
	 */
	private String action = "long2short";
	
	/**
	 * 需要转换的长链接，支持http://、https://、weixin://wxpay 格式的url
	 */
	private String long_url;

	/**
	 * 此处填long2short，代表长链接转短链接
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * 此处填long2short，代表长链接转短链接
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	public String getLong_url() {
		return long_url;
	}

	public void setLong_url(String long_url) {
		this.long_url = long_url;
	}

	@Override
	public String toString() {
		return "ShorturlParamBean [action=" + action + ", long_url=" + long_url + "]";
	}
	
	
}
