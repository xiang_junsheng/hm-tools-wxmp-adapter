package top.hmtools.wxmp.material.model;

import java.util.List;

/**
 * Auto-generated: 2019-08-13 16:24:57
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class NewsBean {

	private List<Articles> articles;

	public void setArticles(List<Articles> articles) {
		this.articles = articles;
	}

	public List<Articles> getArticles() {
		return articles;
	}

}