package top.hmtools.wxmp.core.model.message.enums;

/**
 * 事件KEY值
 * 
 * @author HyboWork
 *
 */
public enum EventKey {

	/**
	 * 扫描带参数二维码事件
	 * <br>用户未关注时，进行关注后的事件推送
	 * <br>事件KEY值，qrscene_为前缀，后面为二维码的参数值
	 */
	qrscene_("qrscene_"),

	/**
	 * 空，null
	 */
	none(null);

	private String name;

	private EventKey(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
