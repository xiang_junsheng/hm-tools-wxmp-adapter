package top.hmtools.wxmp.core.httpclient.responseHandler;

import org.apache.http.client.ResponseHandler;

/**
 * 获取处理httpclient 发送http请求后接收的http response的handler对象实例
 * @author HyboWork
 *
 */
public enum EResponseHandler {

	/**
	 * 将获取的http response body转换为字符串（string）
	 */
	StringResponse(new ResponseHandlerString()),
	/**
	 * 将获取的http response body转换为本地输入流（inputstream，在本地操作系统临时文件缓存）
	 */
	InputStreamResponse(new ResponseHandlerInputStream())
	;
	
	private  ResponseHandler<?> responseHandler;
	
	private <T> EResponseHandler(ResponseHandler<T> rh){
		this.responseHandler = rh;
	}

	public  ResponseHandler<?> getResponseHandler() {
		return responseHandler;
	}
	
	
}
