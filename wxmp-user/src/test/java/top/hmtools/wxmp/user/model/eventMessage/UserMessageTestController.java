package top.hmtools.wxmp.user.model.eventMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;

@WxmpController
public class UserMessageTestController {

	final Logger logger = LoggerFactory.getLogger(UserMessageTestController.class);
	private ObjectMapper objectMapper;
	
	/**
	 * 获取用户地理位置的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void ClickEventMessage(LocationMessage msg){
		this.printFormatedJson("获取用户地理位置的事件推送", msg);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
