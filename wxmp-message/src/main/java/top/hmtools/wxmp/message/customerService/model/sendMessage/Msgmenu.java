package top.hmtools.wxmp.message.customerService.model.sendMessage;

import java.util.List;

/**
 * Auto-generated: 2019-08-25 18:7:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Msgmenu {

	private String head_content;
	private List<ItemList> list;
	private String tail_content;

	public void setHead_content(String head_content) {
		this.head_content = head_content;
	}

	public String getHead_content() {
		return head_content;
	}

	public void setList(List<ItemList> list) {
		this.list = list;
	}

	public List<ItemList> getList() {
		return list;
	}

	public void setTail_content(String tail_content) {
		this.tail_content = tail_content;
	}

	public String getTail_content() {
		return tail_content;
	}

}