package top.hmtools.wxmp.message.eventPush.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 扫描带参数二维码事件 之  2. 用户已关注时的事件推送
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[FromUser]]></FromUserName>
	<CreateTime>123456789</CreateTime>
	<MsgType><![CDATA[event]]></MsgType>
	<Event><![CDATA[SCAN]]></Event>
	<EventKey><![CDATA[SCENE_VALUE]]></EventKey>
	<Ticket><![CDATA[TICKET]]></Ticket>
</xml>
 * }
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.SCAN)
public class QRScanEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4508012084070575843L;

	/**
	 * 二维码的ticket，可用来换取二维码图片
	 */
	@XStreamAlias("Ticket")
	private String ticket;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	
	
}
