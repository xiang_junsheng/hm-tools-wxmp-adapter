package top.hmtools.wxmp.user.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class TagWapperResult extends ErrcodeBean {

	private TagResult tag;

	public TagResult getTag() {
		return tag;
	}

	public void setTag(TagResult tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "TagWapperResult [tag=" + tag + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
